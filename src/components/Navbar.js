import React from "react";

export const Navbar = () => {
    return (
        <div className="navbar">
            <div className="nav-content container">
                <div className="nav-title">
                    <p>demo</p>
                    <p>streaming</p>
                </div>
                <div className="nav-info">
                    <p>log in</p>
                    <p>start your free trial</p>
                </div>
            </div>
        </div>
    )
}


