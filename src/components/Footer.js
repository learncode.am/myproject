import React from "react";
import appstore from '../img/app-store.svg';
import googleplay from '../img/play-store.svg';
import microsoft from '../img/windows-store.svg';
export const Footer = () => {
    return (
        <div className="footer">
            <div className="footer-content container">
                <ul className="footer-ul">
                    <li>home</li>
                    <li>terms and conditions</li>
                    <li>privacy policy</li>
                    <li>collection satement</li>
                    <li>help</li>
                    <li>manage account</li>
                </ul>
                <span>Copyright &copy; 2016 DEMO Streaming. all rights reserved.</span>
                <div className="footer-feedback">
                    <div className="soc-item">
                        <i className="fab fa-facebook"></i>
                        <i className="fab fa-twitter"></i>
                        <i className="fab fa-instagram"></i>
                    </div>
                    <div className="market-item">
                        <div className="appstore">
                            <img src={appstore} alt=""/>
                        </div>
                        <div className="googleplay">
                            <img src={googleplay} alt=""/>
                        </div>
                        <div className="microsoft">
                            <img src={microsoft} alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
