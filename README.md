# Project Title

Stan Coding Challenge

## Description

This is a simple web application that displays a list of popular movies and TV series in JSON format.
this application uses 3 types of content sorter
* The first 21 items are displayed
* Films and TV series that came out after 2010
* Sorted in ascending alphanumeric order
React was used to create this project.

## Getting Started

### Dependencies

* For this application to work properly, you must have Node.js environment installed and all node_modules (react-router-dom, axios)


### Executing program
* Step-by-step bullets
* Go to root file and type

```
npm start
```

## Help

Any advise for common problems or issues.
```
command to run if program contains helper info
```

## Authors

Contributors names and contact info

ex. Dominique Pizzie  
ex. [@DomPizzie](https://twitter.com/dompizzie)

## Version History

* 0.2
    * Various bug fixes and optimizations
    * See [commit change]() or See [release history]()
* 0.1
    * Initial Release

## License

This project is licensed under the [NAME HERE] License - see the LICENSE.md file for details

## Acknowledgments

Inspiration, code snippets, etc.
* [awesome-readme](https://github.com/matiassingers/awesome-readme)
* [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* [dbader](https://github.com/dbader/readme-template)
* [zenorocha](https://gist.github.com/zenorocha/4526327)
* [fvcproductions](https://gist.github.com/fvcproductions/1bfc2d4aecb01a834b46)